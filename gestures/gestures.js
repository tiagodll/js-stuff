function Gestures(selector) {
  var mm = {
    dragging: false,
    starttime: 0,
    start_x: 0,
    start_y: 0,
    current_item: null,
    current_speed_x: 0,
    current_speed_y: 0,
    current_distance_x: 0,
    current_distance_y: 0,
    swipe_event: null,
    drag_event: null,
    click_event: null,
    release_event: null,

    drag_start: (e)=>{
      mm.dragging = true;
      mm.current_item = e.target;
      let is_touch = (e.type == "touchstart");
      mm.start_x = is_touch ? e.touches[0].clientX : e.x;
      mm.start_y = is_touch ? e.touches[0].clientY : e.y;
      mm.starttime = e.timeStamp;
      mm.click_event({ target:mm.current_item });
    },
    drag: (e)=>{
      if(mm.dragging){
        let is_touch = (e.type == "touchmove");
        let x = is_touch ? e.touches[0].clientX : e.x;
        let y = is_touch ? e.touches[0].clienty : e.y;

        mm.current_distance_x = x - mm.start_x;
        mm.current_distance_y = y - mm.start_y;
        mm.drag_event({ 
          target: mm.current_item,
          x: x,
          y: y,
          distance_x: mm.current_distance_x, 
          distance_y: mm.current_distance_y })
      }
    },
    drag_stop: (e)=>{
      mm.dragging = false; 
      mm.current_distance_x = e.x - mm.start_x;
      mm.current_distance_y = e.y - mm.start_y;
      let time = e.timeStamp - mm.starttime;
      mm.current_speed_x = (mm.current_distance_x * 10) / time;
      mm.current_speed_y = (mm.current_distance_y * 10) / time;

      mm.release_event({target:mm.current_item})
      mm.swipe_event({ 
          target: mm.current_item,
          x: e.x,
          y: e.y,
          speed_x: mm.current_speed_x,
          speed_y: mm.current_speed_y,
          distance_x: mm.current_distance_x, 
          distance_y: mm.current_distance_y });
    }
  }
  for(let item of document.querySelectorAll(selector)){
    //mouse
    item.addEventListener("mousedown", e=>{ mm.drag_start(e) });        
    item.addEventListener("mousemove", e=>{ mm.drag(e) });        
    item.addEventListener("mouseup", e=>{ mm.drag_stop(e) });
    document.addEventListener("mouseup", e=>{ mm.drag_stop(e) });

    //finger
    item.addEventListener("touchstart", e=>{ mm.drag_start(e) }, false);
    item.addEventListener("touchend", e=>{ mm.drag_stop(e) }, false);
    item.addEventListener("touchcancel", e=>{ mm.drag_stop(e) }, false);
    item.addEventListener("touchmove", e=>{ mm.drag(e) }, false);
  }

  return mm;
}