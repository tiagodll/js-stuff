# VanillaJS gestures support

very simple and lightweight
demo available in index.html

## how to use

### just add a reference to the lib
```html
<script type="text/javascript" src="gestures.js"></script>
```
### initiate the Gestures lib with the selector for your items
```js
var mm = Gestures(".main .card.note-item");
```

### and add your callbacks
```js
mm.swipe_event = e=>{ animateSlide(e) };
mm.drag_event = e=>{ e.target.style.left = e.distance_x + "px" };
mm.click_event = e=>{ e.target.classList.add("selected") };
mm.release_event = e=>{ e.target.classList.remove("selected") };
```